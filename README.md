# pyChord

<img src="https://gitlab.com/kenzietandun/pyChord/raw/master/screenshot.png" height="600">

A simple-to-use python3 music player & remote

### Installation

- Create a virtualenv `virtualenv -p python3 pyChord`

- `cd pyChord && source bin/activate`

- `pip install -r req.txt`

- Fill in pychord.conf.template, then copy it to ~/.config/pychord.conf. `cp pychord.conf.template ~/.config/pychord.conf`

### Usage

- `python3 frontend.py`

- You should be able to access the remote from http://YOUR_IP:PORT

- Setting volume requires amixer

- or if you want to run it on bootup, edit and copy `pychord.service` to /etc/systemd/system/, then do `systemctl enable pychord.service`

### TODO

- Favourite songs
