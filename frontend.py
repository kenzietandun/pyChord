#!/usr/bin/env python3

import os
import sys
import configparser
import player
from flask import Flask
from flask import render_template
app = Flask(__name__)


@app.route('/', methods=['GET'])
def mainpage():
    return render_template('main.html')

@app.route('/play', methods=['GET'])
def play():
    p.play()
    return render_template('main.html')

@app.route('/pause', methods=['GET'])
def pause():
    p.pause()
    return render_template('main.html')

@app.route('/stop', methods=['GET'])
def stop():
    p.stop()
    return render_template('main.html')

@app.route('/prev', methods=['GET'])
def prev():
    p.prev()
    return render_template('main.html')

@app.route('/next', methods=['GET'])
def next():
    p.next()
    return render_template('main.html')

@app.route('/voldown', methods=['GET'])
def voldown():
    p.dec_vol()
    return render_template('main.html')

@app.route('/volup', methods=['GET'])
def volup():
    p.inc_vol()
    return render_template('main.html')

if __name__ == '__main__':
    p = player.Player()
    cfg = configparser.ConfigParser()
    cfg.read(os.environ['HOME'] + '/.config/pychord.conf')
    try:
        port = int(cfg.get('pychord', 'port'))
    except:
        print('Configuration file not found.')
        sys.exit(1)

    app.run(host='0.0.0.0', port=port)
