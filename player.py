#!/usr/bin/env python3

import configparser
import os
import sqlite3
import vlc
import time
from random import shuffle

class Player():
    def __init__(self):
        cfg = configparser.ConfigParser()
        cfg.read(os.environ['HOME'] + '/.config/pychord.conf')
        try:
            self.music_dir = cfg.get('pychord', 'music_dir')
            self.db = cfg.get('pychord', 'db')
        except:
            print('Configuration file not found.')
            sys.exit(1)

        if not self.music_dir.endswith('/'):
            self.music_dir += '/'

        self.song_list = []
        self.init_db()
        self.get_songlist()
        self.init_playlist()

    def init_db(self):
        if not os.path.isfile(self.db):
            os.mknod(self.db)

            self.conn = sqlite3.connect(self.db)
            self.conn.execute('''CREATE TABLE MUSIC
              (ID TEXT PRIMARY KEY NOT NULL,
               FAV INT);''')

            self.conn.commit()

        else:
            self.conn = sqlite3.connect(self.db)

    def update_music_favscore(self, song, score):
        if score > 5:
            score = 5
        elif score < 0:
            score = 0

        # check if song name is in music directory
        if song in os.listdir(self.music_dir):
            # if it is, check if it is already in database
            c = self.conn.execute("""SELECT ID, FAV FROM MUSIC
                    WHERE ID = '{}'""".format(song))
            
            # if it is in database, update it
            if c.fetchone():
                self.conn.execute("""UPDATE MUSIC set FAV = {}
                   WHERE ID = \'{}\'""".format(score, song))
            # else, create a new entry for song
            else:
                self.conn.execute("""INSERT INTO MUSIC (ID, FAV)
                   VALUES (\'{}\', {})""".format(song, score))

            # commit changes
            self.conn.commit()

    def get_songlist(self):
        songs = [ f for f in os.listdir(self.music_dir) if f.endswith(('.mp3', '.m4a')) ]

        c = self.conn.execute('SELECT ID, FAV FROM MUSIC')
        for m_id, m_fav in c:
            if m_id in songs:
                songs.extend([m_id] * m_fav)
        
        shuffle(songs)
        self.song_list = [ self.music_dir + s for s in songs ]

    def init_playlist(self):
        inst = vlc.Instance()
        self.pl = inst.media_list_player_new()
        media_list = inst.media_list_new(self.song_list)
        self.pl.set_media_list(media_list)

    def play(self):
        playing = set([1, 2, 3, 4])
        self.pl.play()

    def stop(self):
        self.pl.stop()

    def pause(self):
        self.pl.pause()

    def next(self):
        self.pl.next()

    def prev(self):
        self.pl.previous()

    def inc_vol(self):
        os.system('/usr/bin/amixer -q sset Master 5%+')

    def dec_vol(self):
        os.system('/usr/bin/amixer -q sset Master 5%-')

if __name__ == '__main__':
    print('for import only')
